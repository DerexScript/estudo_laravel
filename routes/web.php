<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', [UserController::class, 'create'])->name('register.create');

Route::post('/register', [UserController::class, 'store'])->name('register.store');

Route::get('/login', function () {
    return view('login');
})->name('login.create');

Route::post('/login', [UserController::class, 'auth'])->name('login.auth');

Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard.home');
