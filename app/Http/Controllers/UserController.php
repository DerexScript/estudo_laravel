<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:20',
            'surname' => 'required|max:20',
            'username' => 'required|unique:users|max:20',
            'email' => 'required|unique:users|max:30',
            'password' => 'required|max:20',
        ];
        $errorMessages = [
            'name.required' => "O campo nome é obrigatorio.",
            'name.max' => "O campo nome pode ter no maximo 20 caracteres.",
            'email.unique' => "Já existe uma conta criada com esse e-mail.",
        ];
        $validated = $request->validate($rules, $errorMessages);
        $validated['password'] = Hash::make($validated['password']);

        $user = new User();
        $user->fill($validated);
        if ($user->save()) {
            return redirect()->back();
        }
        return redirect()->back()->withErrors(["register" => "Falha ao registrar usuario"]);

        /*
        $user = new User();
        $user->name = $validated['name'];
        $user->surname = $validated['surname'];
        $user->username = $validated['username'];
        $user->email = $validated['email'];
        $user->password = $validated['password'];
        $user->save();
        */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }


    public function auth(Request $request)
    {
        $user = User::where("username", "=", $request->credential)->orWhere("email", "=", $request->credential)->get();
        if ($user && $user->password == $request->password) {
            Auth::login($user, true);
            if ($request->session()->regenerate()) {
                //intended tenta redirecionar o usuario para rota que estava tentando acessar antes de ser interceptado pelo middleware
                //uma uri pode ser passada como parametro em caso de falha..
                return redirect()->intended('dashboard.home');
            }
        }
        return redirect()->back()->withErrors(["login" => "Usuario ou senha invalidos."]);
    }
}
