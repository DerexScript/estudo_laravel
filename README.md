# Estudo_Laravel

> um simples sistema de cadastro usando laravel, com finalidade de estudo. 

## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:

* Você instalou a versão mais recente do `php, composer e mariadb`

## ☕ Rodando o projeto localmente

Para usar o projeto localmente, siga estas etapas:

```
* primeiro abra seu terminal e navegue até a pasta do projeto pelo terminal.
* crie um novo arquivo .env com o seguinte comando: php -r "copy('.env.example', '.env');"
* gere uma nova APP_KEY com o comando: php artisan key:generate
* agora no arquivo .env que você acabou de criar na pasta raiz da sua aplicação laravel, configure a parte de acesso a database.
* lembre de ter o banco de dados que configurou no .env rodando no host configurado no .env, e lembre tambem de criar a database que configurou no .env
* agora faça suas migrate persistir no banco de dados com o comando: php artisan migrate
* [Se vc seguiu bem as etapas até aqui, o php artisan irar persistir suas migration na base de dados]
* por ultimo faça 'composer install' na pasta do projeto.
```

## 📫 Contribuindo para o projeto

Para contribuir com Finanies, siga estas etapas:

1. Bifurque este repositório.
2. Crie um branch: `git checkout -b <nome_branch>`.
3. Faça suas alterações e confirme-as: `git commit -m '<mensagem_commit>'`
4. Envie para o branch original: `git push origin <nome_do_projeto> / <local>`
5. Crie a solicitação de pull.

Como alternativa, consulte a documentação do GitHub em [como criar uma solicitação pull](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## 🤝 Colaboradores

Agradecemos às seguintes pessoas que contribuíram para este projeto:

<table>
  <tr>
    <td align="center">
      <a href="https://gitlab.com/DerexScript/estudo_laravel">
        <img src="https://gitlab.com/uploads/-/system/user/avatar/1386205/avatar.png?width=128" width="100px;" alt="Foto do Derex no GitHub"/><br>
        <sub>
          <b>Derex</b>
        </sub>
      </a>
    </td>
  </tr>
</table>

## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.

[⬆ Voltar ao topo](#estudo_laravel)<br>