<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Entrar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>

<body>
    <div class="h1 d-flex justify-content-center mt-5">Entrar</div>
    <div class="container mt-5">
        <div class="row">
            <form method="post" action="./login">
                @csrf

                <div class="mb-3">
                    <label for="inputCredential" class="form-label">Credencial</label>
                    <input type="text" name="credential" class="form-control" id="inputCredential" aria-describedby="credentialHelp">
                    <div id="credentialHelp" class="form-text">Informe sua credencial.</div>
                </div>

                <div class="mb-3">
                    <label for="inputPassword" class="form-label">Senha</label>
                    <input type="password" name="password" class="form-control" id="inputPassword">
                </div>
                <button type="submit" class="btn btn-primary">Entrar</button>
            </form>

            <div class="col-12 mt-5">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</body>

</html>