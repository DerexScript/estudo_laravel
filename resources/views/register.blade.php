<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>

<body>
    <div class="h1 d-flex justify-content-center mt-5">Crie um novo usuario.</div>
    <div class="container mt-5">
        <div class="row">
            <form method="post" action="./register">
                @csrf
                <div class="mb-3">
                    <label for="inputName" class="form-label">Nome</label>
                    <input type="text" name="name" class="form-control" id="inputName" aria-describedby="nameHelp">
                    <div id="nameHelp" class="form-text">Informe seu nome.</div>
                </div>

                <div class="mb-3">
                    <label for="inputSurname" class="form-label">Sobrenome</label>
                    <input type="text" name="surname" class="form-control" id="inputSurname" aria-describedby="surnameHelp">
                    <div id="surnameHelp" class="form-text">Informe seu sobrenome.</div>
                </div>

                <div class="mb-3">
                    <label for="inputUsername" class="form-label">Usuario</label>
                    <input type="text" name="username" class="form-control" id="inputUsername" aria-describedby="usernameHelp">
                    <div id="usernameHelp" class="form-text">Informe seu nome de usuario.</div>
                </div>

                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                </div>
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </form>

            <div class="col-12 mt-5">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if ($errors->has('register'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('register') }}
                </div>
                @endif
            </div>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</body>

</html>